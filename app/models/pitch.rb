class Pitch < ActiveRecord::Base
  belongs_to :user
  has_many :comments

  mount_uploader :image, ImageUploader

  def root_comments
    Comment.where(pitch_id: id, parent_id: nil)
  end
end
