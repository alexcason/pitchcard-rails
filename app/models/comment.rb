class Comment < ActiveRecord::Base
  belongs_to :pitch
  belongs_to :user
  has_many :children, class_name: 'Comment'
  belongs_to :parent, class_name: 'Comment'

  def children
    Comment.where(parent_id: id)
  end
end
