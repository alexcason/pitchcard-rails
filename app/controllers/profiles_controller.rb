class ProfilesController < ApplicationController
  before_action :set_user

  def show
  end

  private

  def set_user
    @user = User.find_by username: params[:username]
  end

end
