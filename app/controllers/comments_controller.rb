class CommentsController < ApplicationController
  before_filter :authenticate_user!, except: [:index, :show]
  before_action :set_user, :set_pitch

  def create
    @comment = @pitch.comments.build(comment_params)
    @comment.user_id = current_user.id

    respond_to do |format|
      if @comment.save

        format.json { render json: @comment.to_json(:include => [{ :user => { :methods => :name_or_username, :only => :name_or_username } }, { :pitch => { :only => [:id, :user], :include => {:user => { :only => :username } } } }]), status: :created, location: @pitch }
        # format.html { redirect_to profile_pitch_path(@pitch.user.username, @pitch.id) }
      else
        format.json { render json: @comment.errors, status: :unprocessable_entity }
        # format.html { render action: 'show' }
      end
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:body, :parent_id)
  end

  def set_user
    @user = User.find_by username: params[:username]
  end

  def set_pitch
    @pitch = @user.pitches.find(params[:pitch_id])
  end
end
