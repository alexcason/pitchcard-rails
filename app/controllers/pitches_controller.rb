class PitchesController < ApplicationController
  before_filter :authenticate_user!, except: [:index, :show]
  before_action :set_user, only: [:show]

  def index
  end

  def show
    @pitch = @user.pitches.find(params[:pitch_id])
  end

  def new
  end

  def create
    @pitch = current_user.pitches.build(pitch_params)

    respond_to do |format|
      if @pitch.save
        format.html { redirect_to profile_pitch_path(current_user.username, @pitch.id) }
      else
        format.html { render action: 'new' }
      end
    end
  end

  private

  def pitch_params
    params.require(:pitch).permit(:title, :problem, :solution, :difference, :image)
  end

  def set_user
    @user = User.find_by username: params[:username]
  end

end
