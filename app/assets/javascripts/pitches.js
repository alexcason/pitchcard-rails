var ready = function() {
  var editor = new MediumEditor('.editable');

  $('#pitch_image').change(function() {
    pitch.changeImage(this);
  });

  $('#pitch_title').on('input', function() {
    pitch.validate();
  });

  $('.pc-pitch-container .editable').on('input', function() {
    pitch.updateFormInput($(this));

    pitch.validate();
  });

  $('.pc-pitch-comments-top').on('click', '.pc-pitch-comment-reply', function() {
    pitch.showCommentReplyForm($(this));

    return false;
  });

  $('.pc-pitch-comment-reply-form').focusout(function() {
    pitch.hideCommentReplyForm($(this));
  });

  $('.pc-pitch-container').on('ajax:success', '.pc-pitch-comment-form, .pc-pitch-comment-reply-form', function(e, data, status, xhr) {
    pitch.commentCreated(e, data, status, xhr);
  });

};

var pitch = {
  PLACEHOLDER_TITLE: 'your pitch title',

  commentCreated: function(e, data, status, xhr) {
    try {
      var $elmCommentForm = $(e.target);
      var $elmCommentFormInput = $elmCommentForm.find('textarea');
      var $elmComments = $elmCommentForm.siblings('.pc-pitch-comments');
      var pitchCommentsPath = '/' + data.pitch.user.username + '/' + data.pitch.id + '/comments'
      var commentHTML = '';

      if ($elmCommentForm.hasClass('pc-pitch-comment-form')) {
        // comment created
        commentHTML = '<li class="pc-pitch-comment-top">'
                      +  '<header>'
                      +    '<h1>' + data.user.name_or_username + '</h1>'
                      +  '</header>'
                      +  '<p>' + data.body + '</p>'
                      +  '<div class="row pc-pitch-comment-details">'
                      +    '<div class="col-xs-6">'
                      +      '<span class="text-muted">' + data.created_at + '</span>'
                      +    '</div>'
                      +    '<div class="col-xs-6 text-right">'
                      +      '<a href="#" class="pc-pitch-comment-reply">Reply</a>'
                      +    '</div>'
                      +  '</div>'
                      +  '<div class="pc-pitch-comment-replies">'
                      +    '<ul class="pc-pitch-comments"></ul>'
                      +    '<form accept-charset="UTF-8" action="' + pitchCommentsPath + '" class="pc-pitch-comment-reply-form" data-remote="true" method="post" style="display: none;"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="✓"></div>'
                      +      '<input id="comment_parent_id" name="comment[parent_id]" type="hidden" value="' + data.id + '">'
                      +      '<div class="form-group">'
                      +        '<textarea class="form-control" id="comment_body" name="comment[body]" placeholder="Write a comment..." rows="4"></textarea>'
                      +      '</div>'
                      +      '<div class="text-right">'
                      +        '<button class="btn btn-default" name="button" type="submit">Submit</button>'
                      +      '</div>'
                      +    '</form>'
                      +  '</div>'
                      +'</li>'

        // clear comment form input
        $elmCommentFormInput.val('');

      } else if ($elmCommentForm.hasClass('pc-pitch-comment-reply-form')) {
        // comment reply created
        commentHTML = '<li>'
                      +  '<header>'
                      +    '<h1>' + data.user.name_or_username + '</h1>'
                      +  '</header>'
                      +  '<p>' + data.body + '</p>'
                      +  '<div class="row pc-pitch-comment-details">'
                      +    '<div class="col-xs-6">'
                      +      '<span class="text-muted">' + data.created_at + '</span>'
                      +    '</div>'
                      +    '<div class="col-xs-6 text-right">'
                      +      '<a href="#" class="pc-pitch-comment-reply">Reply</a>'
                      +    '</div>'
                      +  '</div>'
                      +'</li>'

        // clear and hide comment for input
        $elmCommentFormInput.val('');
        $elmCommentForm.hide();

      }

      // add comment to page
      $elmCommentContainer = $elmComments.append(commentHTML);

      // scroll page to view new comment
      pitch.scrollToComment($elmCommentContainer);

    } catch(e) {
      // handle error
    }
  },

  updateFormInput: function($elmEditable) {
    try {
      var input_id = $elmEditable.data('input');
      var content = $elmEditable.html();

      $('#' + input_id).val(content);

    } catch(e) {
      // handle error
    }
  },

  validate: function() {
    try {
      var title = $('#pitch_title').val().toLowerCase();
      var problem = $('#pitchProblem').text();
      var solution = $('#pitchSolution').text();

      var $elmSubmit = $('.pc-pitch-submit button');

      if (title.length > 0 && title != pitch.PLACEHOLDER_TITLE && problem.length > 0 && solution.length > 0) {
        $elmSubmit.removeClass('btn-default').addClass('btn-success');
      } else {
        $elmSubmit.removeClass('btn-success').addClass('btn-default');
      }

    } catch(e) {
      // handle error
    }
  },

  changeImage: function(input) {
    try {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
          $('.pc-pitch-header').css('background-image', 'url(' + e.target.result + ')').addClass('pc-image-set');
          $('.pc-pitch-header .btn').removeClass('btn-default').addClass('btn-success');
        };

        reader.readAsDataURL(input.files[0]);
      }

    } catch(e) {
      // handle error
    }
  },

  showCommentReplyForm: function($elmCommentReplyLink) {
    try {
      $elmCommentReplyForm = $elmCommentReplyLink.closest('.pc-pitch-comment-top').find('.pc-pitch-comment-reply-form');
      $elmCommentReplyInput = $elmCommentReplyForm.find('textarea');

      $elmCommentReplyForm.show();
      $elmCommentReplyInput.focus();

    } catch (e) {
      // handle error
    }
  },

  hideCommentReplyForm: function($elmCommentReplyForm) {
    try {
      $elmCommentReplyInput = $elmCommentReplyForm.find('textarea');

      if ($elmCommentReplyInput.val().length == 0) {
          $elmCommentReplyForm.hide();
      }

    } catch(e) {
      // handle error
    }
  },

  scrollToComment: function($elmCommentContainer) {
    try {
      var commentPosition = $elmCommentContainer.offset().top + $elmCommentContainer.height();
      var viewportHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
      var scrollPosition = commentPosition - viewportHeight + 50;

      $('html, body').animate({
        scrollTop: scrollPosition
      });
    } catch(e) {
      // handle error
    }
  }

}

$(document).ready(ready);
$(document).on('page:load', ready);
