Pitchcard::Application.routes.draw do

  root :to => "home#index"

  devise_for :users

  resources :discover, only: :index
  
  resources :pitches, except: [:new, :show]
  get '/new', to: 'pitches#new'

  get '/:username', to: 'profiles#show', as: 'profile'
  get '/:username/:pitch_id', to: 'pitches#show', as: 'profile_pitch'

  post '/:username/:pitch_id/comments', to: 'comments#create', as: 'profile_pitch_comments'

end
