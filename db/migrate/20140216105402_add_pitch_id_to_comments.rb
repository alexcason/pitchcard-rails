class AddPitchIdToComments < ActiveRecord::Migration
  def change
    add_column :comments, :pitch_id, :integer
  end
end
