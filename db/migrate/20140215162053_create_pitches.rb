class CreatePitches < ActiveRecord::Migration
  def change
    create_table :pitches do |t|
      t.string :title
      t.text :problem
      t.text :solution
      t.text :difference

      t.timestamps
    end
  end
end
