class RemovePrivacyFromPitches < ActiveRecord::Migration
  def change
    remove_column :pitches, :privacy
  end
end
