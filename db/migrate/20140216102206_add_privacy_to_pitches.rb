class AddPrivacyToPitches < ActiveRecord::Migration
  def change
    add_column :pitches, :privacy, :integer, default: 0
  end
end
