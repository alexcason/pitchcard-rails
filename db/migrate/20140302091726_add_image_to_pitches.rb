class AddImageToPitches < ActiveRecord::Migration
  def change
    add_column :pitches, :image, :string
  end
end
